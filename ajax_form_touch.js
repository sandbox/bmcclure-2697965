/**
 * Created by BMcClure on 4/7/2015.
 */

(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.ajaxFormTouch = {
    attach: function(context, options) {
      $('.asaf-control-asaf_submit.form-submit', context).on('touchstart', function (e) {
        var event = options.ajax[$(this).attr('id')]['event'];
        if (event) {
          $(this).trigger(event);
        }
      });
    }
  };
})(jQuery, Drupal, this, this.document);
